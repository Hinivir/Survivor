export const ApiMasurao = async (data_token: string, url: string) => {
    try {
        const response = await fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: "Bearer " + data_token,
                'X-Group-Authorization': process.env.EXPO_PUBLIC_API_KEY || '',
                accept: "application/json",
            },
        });
        if (response.ok) {
            return response.json();
        } else {
            console.error('Error: Calling the Api');
            return null;
        }
    } catch (error) {
        console.error('Error: ' + error);
        return null;
    }
}

export const ApiMasuraoPicture = async (data_token: string, url: string) => {
    try {
        const imageResponse = await fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + data_token,
                'X-Group-Authorization': process.env.EXPO_PUBLIC_API_KEY || '',
                accept: 'application/json',
            },
        });

        if (imageResponse.ok) {
            return await imageResponse.blob();
        } else {
            console.error('Error fetching the picture.');
            return null;
        }
    } catch (error) {
        console.error('Error: ' + error);
        return null;
    }
}
