import React, { useState, useCallback } from 'react';
import { StatusBar } from 'expo-status-bar';
import { RefreshControl, SafeAreaView, ScrollView, StyleSheet, AppRegistry } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { useFonts } from 'expo-font';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from './Page/Login/ApiConnect';
import Home from './Page/Home/Home';
import User from './Page/User/User';
import Trombi from './Page//Widget/Trombinoscope/Trombi';
import TrombiUser from './Page/Widget/Trombinoscope/TrombiUser';
import Weather from './Page/Widget/Weather/Weather';
import News from "./Page/Widget/News/News";
import Movie from './Page/Widget/Movie/Movie';
import Calendar from './Page/Widget/Agenda/Calendar';
import Deepl from './Page/Widget/Deepl/Deepl';

function MainScreen() {
  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = useCallback(() => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <LoginScreen />
        <StatusBar style="auto" />
      </ScrollView>
    </SafeAreaView>
  );
}

export default function App() {
  const Stack = createStackNavigator();
  const [loaded] = useFonts({
    Roboto: require('./assets/font/Roboto-Mono.ttf'),
  });
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Main" component={MainScreen} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="User" component={User} />
        <Stack.Screen name="Trombi" component={Trombi} />
        <Stack.Screen name="TrombiUser" component={TrombiUser} />
        <Stack.Screen name="Weather" component={Weather} />
        <Stack.Screen name="News" component={News} />
        <Stack.Screen name="Movie" component={Movie} />
        <Stack.Screen name="Deepl" component={Deepl} />
        <Stack.Screen name="Calendar" component={Calendar} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '-14.2%',
  },
});

AppRegistry.registerComponent('App', () => App);