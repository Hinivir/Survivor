import { View, Image, FlatList, TouchableOpacity, Dimensions } from 'react-native';
import React from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Card } from 'react-native-paper';
import styles from './Style';

const Home = () => {
    const trombi = require('../../assets/search.png');
    const weather = require('../../assets/cloud.png');
    const news = require('../../assets/newspaper.png');
    const movie = require('../../assets/movie.png');
    const deepl = require('../../assets/translate.png');
    const profile = require('../../assets/account.png');
    const logout = require('../../assets/logout.png');
    const calendar = require('../../assets/calendar.png');
    const buttonList = [
        { id: 0, icon: trombi, title: 'Trombinoscope', route: 'Trombi' },
        { id: 1, icon: weather, title: 'Weather', route: 'Weather' },
        { id: 2, icon: news, title: 'News', route: 'News' },
        { id: 3, icon: movie, title: 'Upcoming Movie', route: 'Movie' },
        { id: 4, icon: deepl, title: 'Deepl', route: 'Deepl' },
        { id: 5, icon: calendar, title: 'Calendar', route: 'Calendar' },
    ];
    const navigation = useNavigation();
    const route = useRoute();
    type RouteParams = {
        data_token?: string;
    };
    const data_token = (route.params as RouteParams)?.data_token;

    const displayButton = () => {
        const renderItem = ({ item }: { item: { id: number, icon: any, title: string, route: string } }) => (
            <Card key={item.id} style={{ width: Dimensions.get('screen').width / 2.5, height: 150, margin: '2O%' }}>
                <TouchableOpacity onPress={() => navigation.navigate(item.route, { data_token: data_token })}>
                    <Card.Title title={item.title} />
                    <Card.Content style={{ alignItems: 'center' }}>
                        <Image source={item.icon} style={{ width: 60, height: 60 }} />
                    </Card.Content>
                </TouchableOpacity>
            </Card>
        );

        return (
            <View style={{ flex: 1, justifyContent: "center", alignContent: "center" }}>
                <FlatList
                    data={buttonList}
                    renderItem={renderItem}
                    keyExtractor={(item) => item.id.toString()}
                    numColumns={2}
                    columnWrapperStyle={{ justifyContent: "center", padding: 10 }}
                    contentContainerStyle={{ alignItems: "center" }}
                />
            </View>
        );
    };

    return (
        <View style={styles.back_size}>
            <View>
                <TouchableOpacity onPress={() => navigation.navigate('User', { data_token: data_token })}>
                    <View style={[styles.buttonTop, { borderRadius: 20, position: 'absolute', left: 5, marginTop: 10 }]}>
                        <Image source={profile} style={styles.buttonImage} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.popToTop()}>
                    <View style={[styles.buttonTop, { borderRadius: 20, position: "absolute", right: 5, marginTop: 10 }]}>
                        <Image source={logout} style={styles.buttonImage} />
                    </View>
                </TouchableOpacity>
            </View>
            <View style={styles.top_button}>
                {displayButton()}
            </View>
        </View>
    );
};

export default Home;
