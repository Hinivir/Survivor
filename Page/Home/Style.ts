import { StyleSheet, Dimensions } from "react-native";

const styles = StyleSheet.create({
    button_style: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    position_button: {
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    back_size: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#CFBCF9',
    },
    top_button: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        borderRadius: 100,
        marginTop: 60,
        width: Dimensions.get('window').width,
    },
    button: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonTop: {
        width: 40,
        height: 40,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonImage: {
        width: 30,
        height: 30,
    },
});

export default styles;