import { Image, Button, Text, TextInput, View, Alert, TouchableOpacity } from 'react-native';
import React, { useState } from 'react';
import { useNavigation } from "@react-navigation/native";
import styles from './Style';

const logo = require('../../assets/favicon.png');

const LoginScreen: React.FC = () => {
    const [email, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const navigation = useNavigation();

    const handleLogin = async () => {
        try {
            const apiUrl = 'https://masurao.fr/api/employees/login';
            const apiKey = process.env.EXPO_PUBLIC_API_KEY;
            const response = await fetch(apiUrl, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-Group-Authorization': apiKey,
                    accept: "application/json",
                },
                body: JSON.stringify({
                    "email": email,
                    "password": password,
                }),
            });
            if (response.status === 200) {
                const data_token = await response.json();
                const token = data_token.access_token;
                navigation.navigate('Home', { data_token: token });
            } else {
                Alert.alert("Login failed");
            }
        } catch (error) {
            console.error('Login error:', error);
            Alert.alert("Login error");
        }
    };

    return (
        <View style={styles.containerback}>
                <View style={styles.grid}>
                    <View style={styles.posimg}>
                        <Image source={logo} style={styles.posimg} />
                    </View>
                    <Text style={styles.header}>Login</Text>
                    <TextInput
                        style={styles.input}
                        placeholderTextColor={'white'}
                        placeholder="Username"
                        onChangeText={(text) => setUsername(text)}
                        value={email}
                    />
                    <TextInput
                        style={styles.input2}
                        placeholderTextColor={'white'}
                        placeholder="Password"
                        onChangeText={(text) => setPassword(text)}
                        value={password}
                        secureTextEntry
                    />
                    <TouchableOpacity style={styles.button}>
                        <Button
                            color={'blue'}
                            title="Login"
                            onPress={handleLogin}
                        />
                    </TouchableOpacity>
                </View>
        </View>
    );
}

export default LoginScreen;
