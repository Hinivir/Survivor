import { StyleSheet, Dimensions } from "react-native";

const styles = StyleSheet.create({
    grid: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        marginBottom: 20,
    },
    container: {
        flex: 1,
        opacity: 1,
        padding: '75%',
        margin: 'auto',
    },
    containerback: {
        flex: 1,
        opacity: 1,
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        backgroundColor: '#CFBCF9',
    },
    posimg: {
        height: 100,
        width: 100,
    },
    header: {
        color: 'black',
        fontSize: 25,
        marginBottom: 20,
        marginTop: 20,
    },
    content: {
        alignItems: 'center',
        marginTop: 20,
        marginLeft: -380,
    },
    text: {
        color: 'white'
    },
    input: {
        width: 200,
        height: 50,
        borderColor: 'white',
        color: 'black',
        textAlign: 'center',
        borderWidth: 3.5,
        marginBottom: 10,
        padding: 12,
        fontSize: 19,
    },
    input2: {
        width: 150,
        height: 50,
        borderColor: 'white',
        color: 'black',
        textAlign: 'center',
        borderWidth: 3.5,
        marginBottom: 10,
        padding: 12,
        fontSize: 19,
    },
    button: {
        width: 100,
        height: 50,
    }
});

export default styles;