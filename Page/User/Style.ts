import { StyleSheet, Dimensions } from "react-native";

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#CFBCF9',
        flex: 1,
        opacity: 1,
        margin: 'auto',
    },
    containerback: {
        flex: 1,
        height: Dimensions.get('screen').height,
        width: Dimensions.get('screen').width,
    },
    postext: {
        margin: '2.6%',
        opacity: 0.5,
    },
    posimg: {
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },
    custom_text: {
        color: 'black',
        fontSize: 20,
    }
});

export default styles;