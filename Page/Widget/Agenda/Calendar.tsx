import React, {useState} from 'react';
import { TextInput, View, TouchableOpacity, StatusBar } from 'react-native';
import { Agenda } from 'react-native-calendars';
import { Card } from 'react-native-paper';
import styles from './Style';

const timeToString = (time) => {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
}

const Calendar = () => {
     const [items, setItems] = React.useState({});
     const [text, setText] = useState('');

    const loadItems = (day) => {

        setTimeout(() => {
            for (let i = -15; i < 85; i++) {
                const time = day.timestamp + i * 24 * 60 * 60 * 1000;

                const strTime = timeToString(time);

                if (!items[strTime]) {
                    items[strTime] = [];

                    const numItems = Math.floor(Math.random() + 1);
                    for (let j = 0; j < numItems; j++) {
                        items[strTime].push({
                            name: '',
                        });
                    }
                }
            }
            const newItems = {};
            Object.keys(items).forEach(key => {
                newItems[key] = items[key];
            });
            setItems(newItems);
        }, 1000);
    }

    const renderItem = (item) => {
        return (
            <TouchableOpacity style={styles.item}>
                <Card>
                    <Card.Content>
                        <View>
                        <TextInput
                            key={item.name}
                            style={{height: 40}}
                            placeholder="Type here"
                            onChangeText={newText => setText(newText)}
                    />
                        </View>
                    </Card.Content>
                </Card>
            </TouchableOpacity>
        );
    }

    return (
        <View style={styles.container}>
        <Agenda
            items={items}
            loadItemsForMonth={loadItems}
            selected={'2023-14-09'}
            showClosingKnob={true}
            refreshing={false}
            renderItem={renderItem}
        />
        <StatusBar />
    </View>
    )
}

export default Calendar