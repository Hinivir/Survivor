import { View, Text, Button, TextInput } from 'react-native';
import SelectDropdown from 'react-native-select-dropdown'
import React, { useState } from 'react';
import { Card } from 'react-native-paper';
import styles from './Style';

const Deepl = () => {

    const [targetLang, setTargetLang] = useState<string>('DE');
    const [text, setText] = useState<string>('');
    const [translatedText, setTranslatedText] = useState<string>('');

    const langAvailable = ['DE', 'EN', 'FR', 'ES', 'IT', 'NL', 'PL', 'PT', 'RU', 'FI'];

    const request = async () => {
        try {
            const response = await fetch('https://api-free.deepl.com/v2/translate', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'DeepL-Auth-Key e225a8ad-d159-de80-4577-a61f44643d72:fx'
                },
                body: JSON.stringify({
                    text: [text],
                    target_lang: targetLang
                })
            });

            const responseData = await response.text();
            const data = JSON.parse(responseData);
            setTranslatedText(data.translations[0].text);
        } catch (error) {
            console.error(error);
        }
    }

    return (
        <View style={styles.default}>
                <Card style={styles.cardTitle}>
                    <Card.Content>
                        <Text style={styles.text}>Deepl</Text>
                        <Text style={styles.text}>Type the text to translate: </Text>
                        <TextInput onChangeText={setText} style={styles.text}></TextInput>
                        <Text style={styles.text}>Select Lang for the translation: </Text>
                        <SelectDropdown
                            dropdownStyle={styles.dropdown}
                            buttonStyle={styles.dropdownButton}
                            rowTextStyle={styles.dropdownText}
                            data={langAvailable}
                            onSelect={(selectedItem) => setTargetLang(selectedItem)}
                            buttonTextAfterSelection={(selectedItem) => {
                                return selectedItem
                            }}
                            rowTextForSelection={(item) => {
                                return item
                            }}
                        />
                        <Button title="Translate" onPress={request} />
                        {translatedText ? (
                            <>
                                <Text style={styles.text}>Translated text: </Text>
                                <Text style={styles.text}>{translatedText}</Text>
                            </>
                        ) : null}
                    </Card.Content>
                </Card>
        </View>
    );
}

export default Deepl;