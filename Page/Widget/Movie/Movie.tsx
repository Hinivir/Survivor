import { FlatList, View, Image, Linking, Text, TouchableOpacity } from 'react-native';
import React, { useState, useEffect } from 'react';
import styles from './Style';
import { Card } from 'react-native-paper';

const Movie = () => {
    const [movie, setMovie] = useState([]);

    const handleApi = async () => {
        const response = await fetch('https://api.themoviedb.org/3/movie/upcoming?page=1', {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + process.env.EXPO_PUBLIC_API_MOVIE_KEY,
                accept: 'application/json'
            }
        });
        const data = await response.json();
        setMovie(data.results);
    }
    useEffect(() => {
        handleApi();
    }, []);
    const keyExtractor = (item, index) => index.toString();
    const renderItem = ({ item }) => {
        const handleMovieDetails = () => {
            const tmdbUrl = `https://www.themoviedb.org/movie/${item.id}`;
            Linking.openURL(tmdbUrl).catch((err) => console.error('An error occurred: ', err));
        };
        return (
                <Card style={styles.card}>
                    <Card.Content>
                    <View style={{ alignItems: 'center'}}>
                        <Image
                            style={styles.poster}
                            source={{ uri: `https://image.tmdb.org/t/p/w500/${item.poster_path}` }}
                            />
                    </View>
                        <Text style={styles.text}>Tittle: {item.original_title}</Text>
                        <Text style={styles.text}>Overview: {item.overview}</Text>
                        <Text style={styles.text}>Date de Sortie: {item.release_date}</Text>
                        <TouchableOpacity onPress={handleMovieDetails} style={{ alignItems: 'center'}}>
                            <Text style={styles.button}>View on The Movie Database</Text>
                        </TouchableOpacity>
                    </Card.Content>
                </Card>
        );
    }
    return (
        <View style={styles.container}>
               <FlatList
                    data={movie}
                    keyExtractor={keyExtractor}
                    renderItem={renderItem}
                    />
        </View>
    )
};

export default Movie;
