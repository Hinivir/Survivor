import { StyleSheet, Dimensions } from "react-native";


const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('screen').width,
        backgroundColor: '#CFBCF9',
    },
    text: {
        color: 'white',
    },
    poster: {
        width: 200,
        height: 300,
    },
    button: {
        color: 'blue',
        textDecorationLine: 'underline',
        marginTop: 10,
    },
        card: {
        margin: 10,
        padding: 10,
        borderRadius: 10,
        elevation: 3,
        backgroundColor: 'rgba(207, 188, 249, 0.6)',
        opacity: 0.8,
        transform: [{ perspective: 1000 }, { rotateX: '2deg' }, { rotateY: '2deg' }],
    },
    cardTitle: {
        margin: 10,
        borderRadius: 10,
        elevation: 3,
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        height: Dimensions.get('window').height - 170,
    },
});

export default styles;