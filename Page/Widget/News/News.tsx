import { FlatList, View, Text, Button, Linking } from 'react-native';
import SelectDropdown from 'react-native-select-dropdown'
import React, { useState } from 'react';
import { Dimensions } from 'react-native';
import { Card } from 'react-native-paper';
import styles from './Style';

const News = () => {
    const lang = ['FR', 'US', 'GB', 'JP', 'DE', 'IT', 'NL', 'NO', 'PT', 'RU', 'SE', 'CN', 'TW', 'HK', 'IN', 'AR', 'BR', 'CA'];
    const [country, setCountry] = useState('');
    const [news, setNews] = useState([]);
    const request = async () => {
        console.log(country);
        const response = await fetch('https://newsapi.org/v2/top-headlines?country=' + country, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'x-api-key': process.env.EXPO_PUBLIC_API_NEWS_KEY,
                'accept': 'application/json'
            }
        });
        const data = await response.json();
        setNews(data.articles)
    }
    const keyExtractor = (item, index) => index.toString();

    const renderItem = ({ item }) => (
        <Card style={styles.card}>
            <Card.Content>
                <Text style={styles.title}>{item.title}</Text>
                <Text style={styles.date}>Date: {new Date(item.publishedAt).toLocaleString()}</Text>
                <Button
                    title='Read more'
                    onPress={() => Linking.openURL(item.url)}
                />
            </Card.Content>
        </Card>
    );
    return (
        <View style={{height: Dimensions.get('screen').height, backgroundColor: '#CFBCF9',}}>
                <Card style={styles.cardTitle}>
                    <Card.Content>
            <Text style={styles.text}>Select Lang for the news: </Text>
                <SelectDropdown
                    dropdownStyle={styles.dropdown}
                    buttonStyle={styles.dropdownButton}
                    rowTextStyle={styles.dropdownText}
                    defaultButtonText='Select country'
                    data={lang}
                    onSelect={(selectedItem, index) => {
                        setCountry(selectedItem);
                    }}
                    buttonTextAfterSelection={(selectedItem, index) => {
                        return selectedItem
                    }}
                    rowTextForSelection={(item, index) => {
                        return item
                    }}
                />
                <Button
                    title='Search'
                    onPress={request}
                />
                <FlatList
                    style={{height: Dimensions.get('screen').height - 300}}
                    data={news}
                    keyExtractor={keyExtractor}
                    renderItem={renderItem}
                />
                    </Card.Content>
                </Card>
        </View>
    )
}

export default News;
