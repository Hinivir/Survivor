import { StyleSheet, Dimensions } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        opacity: 1,
        margin: 'auto',
        backgroundColor: '#CFBCF9',
    },
    containerback: {
        height: Dimensions.get('screen').height,
        width: Dimensions.get('screen').width,
    },
    button: {
        alignItems: 'center',
    },
    trombi: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        alignContent: 'center',
        justifyContent: 'center',
        width: Dimensions.get('screen').width / 2,
    },
    postext: {
        margin: '2.6%',
        paddingLeft: 'auto',
        opacity: 0.5,
    },
    posimg: {
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },
    custom_text: {
        color: 'black',
        fontSize: 25,
    },
    cardTitle: {
        elevation: 3,
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        height: Dimensions.get('window').height - 60,
        width: Dimensions.get('window').width,
    }
});

export default styles;