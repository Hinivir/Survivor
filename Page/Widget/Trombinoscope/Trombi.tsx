import { FlatList, Text, View, Image, TouchableOpacity, ImageBackground } from 'react-native';
import React, { useState, useEffect } from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import { ApiMasurao, ApiMasuraoPicture } from '../../../ApiAbstraction';
import { Card } from 'react-native-paper';
import styles from './Style';

const Trombi = () => {
    const navigation = useNavigation();
    const route = useRoute();
    const data_token = route.params?.data_token;

    const [data, setData] = useState([]);
    const [imageData, setImageData] = useState({});

    useEffect(() => {
        const handleAllUsers = async () => {
            const data = await ApiMasurao(data_token, 'https://masurao.fr/api/employees')
            if (data != null) {
                setData(data);
            } else {
                console.error('Error: Error cannot get all the employees');
            }
        }
        handleAllUsers();
    }, [data_token]);
    useEffect(() => {
        const loadImages = async () => {
            const imagePromises = data.map(async (employee) => {
                const blob = await ApiMasuraoPicture(data_token, `https://masurao.fr/api/employees/${employee.id}/image`);
                if (blob != null) {
                    const reader = new FileReader();
                    reader.onload = () => {
                        const base64data = reader.result.split(',')[1];
                        setImageData((prevImageData) => ({
                            ...prevImageData,
                            [employee.id]: `data:image/jpeg;base64,${base64data}`,
                        }));
                    };
                    reader.readAsDataURL(blob);
                } else {
                    console.error(`Failed to fetch image for employee ID: ${employee.id}`);
                }
            });
            await Promise.all(imagePromises);
        };
        loadImages();
    }, [data, data_token]);
    const renderEmployeeImage = (employee) => {
        return (
            <View>
                <TouchableOpacity
                onPress={() =>
                    navigation.navigate('TrombiUser', { data_token: data_token, id: employee.id })
                }>
                <View style={styles.trombi}>
                    <View style={styles.button} key={employee.id}>
                            <Text style={{color: 'white', fontSize: 20}}>{employee.name} {employee.surname}</Text>
                            <Image
                                style={{ width: 100, height: 100, borderRadius: 25  }}
                                source={{ uri: imageData[employee.id] }}
                                />
                        </View>
                                </View>
                </TouchableOpacity >
            </View>
        );
    };
    return (
        <View style={styles.container}>
            <View>
                <Card style={styles.cardTitle}>
                    <Card.Content>
                    <FlatList
                        numColumns={2}
                        data={data}
                        keyExtractor={(item) => item.id.toString()}
                        renderItem={({ item }) => renderEmployeeImage(item)}
                    />
                    </Card.Content>
                 </Card>
            </View>
        </View>
    );
}

export default Trombi;
