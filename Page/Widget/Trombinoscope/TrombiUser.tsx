import React, { useState, useEffect } from 'react';
import { Image, Text, View } from 'react-native';
import { useRoute } from "@react-navigation/native";
import { ApiMasurao, ApiMasuraoPicture } from '../../../ApiAbstraction';
import styles from './Style';
import { Card } from 'react-native-paper';

const TrombiUser = () => {
    const route = useRoute();
    const data_token = route.params?.data_token;
    const id = route.params?.id;
    const [name, setName] = useState('');
    const [surname, setSurname] = useState('');
    const [email, setEmail] = useState('');
    const [work, setWork] = useState('');
    const [gender, setGender] = useState('');
    const [birth_date, setBirth_date] = useState('');
    const [imageData, setImageData] = useState(null);

    const GetUserInfo = async () => {
        const data = await ApiMasurao(data_token, 'https://masurao.fr/api/employees/' + id);
        if (data != null) {
            setName(data.name);
            setSurname(data.surname);
            setEmail(data.email);
            setWork(data.work);
            setGender(data.gender);
            setBirth_date(data.birth_date);
        } else {
            console.error('Error fetching the user information.');
        }
    }
    useEffect(() => {
        const fetchImage = async () => {
            const blob = await ApiMasuraoPicture(data_token, 'https://masurao.fr/api/employees/' + id + '/image');
            if (blob != null) {
                const reader = new FileReader();
                reader.onload = () => {
                    const base64data = reader.result.split(',')[1];
                    setImageData(`data:image/jpeg;base64,${base64data}`);
                };
                reader.readAsDataURL(blob);
            } else {
                console.error('Error fetching the picture of the employee.')
            }
        };
        fetchImage();
    }, [data_token]);
    GetUserInfo();
    return (
        <View style={styles.container}>
                <View style={styles.posimg}>
                    {imageData && <Image source={{ uri: imageData }} style={{ width: 200, height: 200, margin: 60, borderRadius: 25  }} />}
                </View>
                <Card style={styles.postext}>
                    <Card.Content>
                    <Text style={styles.custom_text}>Name: {name}</Text>
                    <Text style={styles.custom_text}>Surname: {surname}</Text>
                    <Text style={styles.custom_text}>Email: {email}</Text>
                    <Text style={styles.custom_text}>Gender: {gender}</Text>
                    <Text style={styles.custom_text}>Birth date: {birth_date}</Text>
                    <Text style={styles.custom_text}>Position: {work}</Text>
                    </Card.Content>
                </Card>
        </View>
    );
}

export default TrombiUser;
