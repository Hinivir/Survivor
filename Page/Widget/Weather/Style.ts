import { StyleSheet, Dimensions } from "react-native";

const styles = StyleSheet.create({
    container: {
        resizeMode: 'cover',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#CFBCF9',
    },
    opacityCase: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    text: {
        color: 'black',
        fontSize: 18,
        margin: 10,
        fontFamily: 'Roboto',
    },
    text_flat: {
        color: 'black',
        fontSize: 16,
        fontFamily: 'Roboto',
        fontWeight: 'bold'
    },
    card: {
        margin: 10,
        padding: 10,
        borderRadius: 10,
        elevation: 3,
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        transform: [{ perspective: 1000 }, { rotateX: '2deg' }, { rotateY: '2deg' }],
    },
    cardTitle: {
        margin: 10,
        borderRadius: 10,
        elevation: 3,
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        height: Dimensions.get('window').height - 70,
    },
    flatList: {
        alignItems: 'center',
        justifyContent: 'center',
    }
});

export default styles;