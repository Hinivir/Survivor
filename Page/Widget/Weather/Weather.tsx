import { FlatList, View, Image, TextInput, Button, Text, TouchableOpacity } from 'react-native';
import React, { useState } from 'react';
import { Card } from 'react-native-paper';
import styles from './Style';

const Weather = () => {

    const [city, setCity] = useState('');
    const [hourlyData, setHourlyData] = useState([]);

    const handleApi = async () => {
        const days = "1";
        const response = await fetch('https://api.weatherapi.com/v1/forecast.json?q=' + city + '&days=' + days + '&aqi=no&alert=no', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'key': process.env.EXPO_PUBLIC_API_WEATHER_KEY,
                'accept': 'application/json'
            }
        });
        const data = await response.json();
        setHourlyData(data.forecast.forecastday[0].hour);
    }
    const keyExtractor = (item, index) => index.toString();
    const renderItem = ({ item }) => (
        <Card style={styles.card}>
            <Card.Content>
            <TouchableOpacity style={styles.opacityCase}>
                <Text style={styles.text_flat}>Time: {item.time}</Text>
                <Text style={styles.text_flat}>Temperature: {item.temp_c}°C</Text>
                <Image
                    source={{ uri: `https:${item.condition.icon}` }}
                    style={{ width: 64, height: 64 }}
                    />
            </TouchableOpacity>
            </Card.Content>
        </Card>
    );
    return (
        <View style={styles.container}>
                    <Card style={styles.cardTitle}>
                        <Card.Content>
                    <TextInput
                        style={styles.text}
                        placeholder='Type a City'
                        placeholderTextColor={'black'}
                        onChangeText={(text) => setCity(text)}
                    />
                    <Button
                        title='Search'
                        onPress={handleApi}
                    />
                        <FlatList
                            data={hourlyData}
                            keyExtractor={keyExtractor}
                            renderItem={renderItem}
                            contentContainerStyle={styles.flatList}
                            />
                        </Card.Content>
                    </Card>
        </View>
    )
};

export default Weather;
